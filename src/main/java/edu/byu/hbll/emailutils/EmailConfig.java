package edu.byu.hbll.emailutils;

import java.io.IOException;
import java.nio.file.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.byu.hbll.config.YamlLoader;

/**
 * Email configuration class. This loads configuration from Yaml files and builds the EmailUtils
 * class object.
 * 
 * @author tnelsonw
 *
 */
public class EmailConfig {

  private static final Logger logger = LoggerFactory.getLogger(EmailConfig.class);

  private JsonNode config;

  private EmailUtils utils;

  /**
   * Default constructor
   */
  public EmailConfig() {
    // do nothing
  }

  /**
   * Loads configuration data read from the given YAML config file(s).
   * 
   * @param configFiles the files from which to read configuration
   * @throws IOException if the given files are not readable or cannot be parsed as YAML
   */
  public void loadConfigFrom(Path... configFiles) throws IOException {
    config = new YamlLoader().load(configFiles);
    loadConfigFromJsonNode(config);
  }

  /**
   * Loads configuration data through a JsonNode object rather than a YAML file.
   *
   * @param config the JsonNode containing the relevant email information.
   * @throws IOException if the ObjectMapper cannot map the config.
   */
  public void loadConfigFromJsonNode(JsonNode config) throws IOException {
    this.config = config;
    ObjectMapper objectMapper = new ObjectMapper();

    if (config == null || config.size() == 0) {
      logger.warn("configuration is empty");
      throw new RuntimeException("configuration is empty");
    }

    JsonNode emailConfig = config.path("email");
    utils = objectMapper.treeToValue(emailConfig, EmailUtils.class);
  }

  /**
   * Gets the EmailUtils object.
   * 
   * @return the EmailUtils object.
   */
  public EmailUtils getEmailUtils() {
    return utils;
  }

  /**
   * Simple getter method.
   *
   * @return The subject of the message to send.
   */
  public String getSubject() {
    return config.path("subject").asText("");
  }

  /**
   * Simple getter method.
   *
   * @return The email of the person sending the message.
   */
  public String getFromWhom() {
    return config.path("fromWhom").asText("");
  }

  /**
   * Simple getter method.
   *
   * @return The JsonNode containing the recipients of the message.
   */
  public String getToWhom() {
    return config.path("toWhom").asText("");
  }

  /**
   * Simple getter method
   *
   * @return The content of the message.
   */
  public String getMessageText() {
    return config.path("messageText").asText("");
  }

}
