package edu.byu.hbll.emailutils;

import java.io.IOException;
import java.util.Optional;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * This class simply sends an email from one user to another where the information needed is stored
 * in a configuration file.
 * 
 * @author tnelsonw
 *
 */
@JsonDeserialize(using = EmailUtils.Deserializer.class)
public class EmailUtils {

  private static final Logger logger = LoggerFactory.getLogger(EmailUtils.class);

  private String username;
  private String password;
  private String toWhom;
  private String fromWhom;
  private String subject;
  private String host;
  private String port;
  private String protocol;
  private String messageText;
  private Properties prop;
  private Session session;

  /**
   * This constructor assigns all of its fields through a builder object defined within this class.
   * 
   * @param builder The builder object containing all the information for this class.
   */
  private EmailUtils(Builder builder) {
    this.username = builder.username;
    this.password = builder.password;
    this.host = builder.host;
    this.port = builder.port;
    this.protocol = builder.protocol;
    prop = new Properties();
    prop.put("mail.smtp.auth", "true");
    prop.put("mail.smtp.starttls.enable", "true");
    prop.put("mail.smtp.host", host);
    prop.put("mail.smtp.port", port);
    prop.setProperty("mail.smtp.port", port);
    this.session = Session.getInstance(prop, new javax.mail.Authenticator() {
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
      }
    });
  }

  /**
   * Simple getter method
   *
   * @return the username field
   */
  public String getUsername() {
    return username;
  }

  /**
   * Simple getter method
   *
   * @return the password field
   */
  public String getPassword() {
    return password;
  }

  /**
   * Simple getter method
   *
   * @return the protocol field
   */
  public String getProtocol() {
    return protocol;
  }

  /**
   * Simple getter method
   *
   * @return the host field
   */
  public String getHost() {
    return host;
  }

  /**
   * Simple getter method
   *
   * @return The toWhom field
   */
  public String getToWhom() {
    return toWhom;
  }

  /**
   * Simple getter method
   *
   * @return The fromWhom field
   */
  public String getFromWhom() {
    return fromWhom;
  }

  /**
   * Simple getter method
   *
   * @return The subject field
   */
  public String getSubject() {
    return subject;
  }

  /**
   * Simple getter method
   *
   * @return The port field
   */
  public String getPort() {
    return port;
  }

  /**
   * Simple getter method
   *
   * @return The messageText field
   */
  public String getMessageText() {
    return messageText;
  }

  /**
   * Simple getter method
   *
   * @return The session field.
   */
  public Session getSession() { return  session;}

  /**
   * Setter method for non-deserialized field
   *
   * @param fromWhom The email of the person sending the message.
   */
  public void setFromWhom(String fromWhom) {
    this.fromWhom = fromWhom;
  }

  /**
   * Setter method for non-deserialized field.
   *
   * @param subject The subject of the message to send.
   */
  public void setSubject(String subject) {
    this.subject = subject;
  }

  /**
   * Setter method for non-deserialized field.
   *
   * @param toWhom The recipients of the email.
   */
  public void setToWhom(String toWhom) {
    this.toWhom = toWhom;
  }

  /**
   * Setter method for non-deserialized field.
   *
   * @param messageText The content of the message.
   */
  public void setMessageText(String messageText) {
    this.messageText = messageText;
  }

  /**
   * Verifies that the given string is non-null and contains at least one non-whitespace character.
   *
   * @param str the string to validate
   * @return trimmed version of the given string
   * @throws IllegalArgumentException if the given string is {@code null}, empty, or blank
   *         (containing only whitespace characters)
   */
  private static String requireNonBlank(String str) {
    return trimToOptional(str).orElseThrow(()
            -> new IllegalArgumentException("Field cannot be null or blank"));
  }

  /**
   * Transforms the provided string such that if it is null or contains only whitespace characters,
   * an empty {@link Optional} will be returned, otherwise a non-empty Optional containing the
   * trimmed form of the string will be returned.
   *
   * @param str the string to transform
   * @return the resulting string as described
   */
  private static Optional<String> trimToOptional(String str) {
    return Optional.ofNullable(str)
            .map(String::trim)
            .filter(s -> !s.isEmpty());
  }

  /**
   * Sends an email using the configuration.
   *
   * @return The sent message. Null if it did not send.
   */
  public Message send() {

    Message message = null;
    try {
      message = new MimeMessage(session);
      message.setFrom(new InternetAddress(fromWhom));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toWhom));
      message.setSubject(subject);
      message.setText(messageText);

      return send(message);
    } catch (MessagingException e) {
      logger.error(e.getMessage());
    }
    return message;
  }

  /**
   * Uses the Transport class to send a message to a server.
   *
   * @param msg The message to send.
   * @return The sent message. Null if it did not send.
   */
  public Message send(Message msg) {
    try {
      Transport.send(msg);
      logger.info("Message sent successfully...");

    } catch (MessagingException e) {
      logger.error(e.getMessage());
      return null;
    }
    return msg;
  }

  /**
   * This class is a builder class used when serializing a yaml file to the EmailUtils POJO
   * 
   * @author tnelsonw
   *
   */
  public static final class Builder {

    private String username;
    private String password;
    private String host;
    private String port;
    private String protocol;

    /**
     * Constructor for this class
     * 
     * @param username Requires at least a username for the constructor.
     */
    public Builder(String username) {
      this.username = requireNonBlank(username);
    }

    /**
     * Setter method for the port field
     * 
     * @param port the value to set to the field
     * @return the builder object
     */
    public Builder port(String port) {
      requireNonBlank(port);
      this.port = port;
      return this;
    }

    /**
     * Setter method for the host field
     * 
     * @param host the value to set to the field
     * @return the builder object
     */
    public Builder host(String host) {
      requireNonBlank(host);
      this.host = host;
      return this;
    }

    /**
     * Setter method for the password field
     * 
     * @param password the value to set to the field
     * @return the builder object
     */
    public Builder password(String password) {
      requireNonBlank(password);
      this.password = password;
      return this;
    }

    /**
     * Setter method for the protocol field.
     *
     * @param protocol the value to set to the field
     * @return the builder object
     */
    public Builder protocol(String protocol) {
      requireNonBlank(protocol);
      this.protocol = protocol;
      return this;
    }

    /**
     * Constructs an EmailUtils object from the builder
     * 
     * @return a newly constructed EmailUtils object
     */
    public EmailUtils build() {
      return new EmailUtils(this);
    }

  }

  /**
   * This class is used by the EmailConfig class to convert the email portion within the config.yml
   * file into a EmailUtils.Builder POJO.
   * 
   * @author tnelsonw
   *
   */
  public static class Deserializer extends JsonDeserializer<EmailUtils> {

    /**
     * Deserializes JSON or Yaml information into a EmailUtils.Builder object
     * 
     * @return the EmailUtils object built from the EmailUtils.Builder class
     */
    @Override
    public EmailUtils deserialize(JsonParser parser, DeserializationContext context)
        throws IOException, JsonProcessingException {
      JsonNode json = parser.readValueAsTree();

      EmailUtils.Builder builder = new EmailUtils.Builder(json.path("username").asText(null));
      Optional.ofNullable(json.path("password").asText(null)).ifPresent(builder::password);
      Optional.ofNullable(json.path("port").asText(null)).ifPresent(builder::port);
      Optional.ofNullable(json.path("host").asText(null)).ifPresent(builder::host);
      Optional.ofNullable(json.path("mail.store.protocol").asText(null))
              .ifPresent(builder::protocol);

      return builder.build();
    }

  }

}

